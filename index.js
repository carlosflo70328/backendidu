const express = require('express');
const morgan = require("morgan")
// const { getConnection } = require('./db/db-connection');
require('dotenv').config();

const cors = require('cors');

const app = express();

const port = process.env.PORT;

// getConnection();

app.use(cors());

//parseo json
app.use(express.json());
app.use(morgan("dev"))
app.use('/', require('./router/tasks'));

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
});

