const { Router } = require("express");
const db = require("../db/db-connection")
const router = Router();

router.post("/",async function (req, res) {
  const {cedula, nombre, tarea} = req.body
  try {
    await db.query("INSERT INTO task (id,name, task) VALUES ($1, $2, $3)", [cedula, nombre, tarea])
    res.send("Guardado");
  } catch (error) {
    console.log(error);
    res.send("Ocurio un error");
  }
}

);

router.get("/", async function (req, res) {
  try {
    const resul = await db.query("SELECT * FROM task")
    res.send(resul.rows);

  } catch (error) {
    console.log(error);
    res.send("Ocurio un error");
  }
});

router.get("/:id", async function (req, res) {
  const { id } = req.params;
  try {

    const result = await db.query("SELECT * FROM task WHERE id = $1", [id]);

    if (result.rows.length === 0)
      return res.status(404).json({ message: "Task not found" });

    res.json(result.rows[0]);
  } catch (error) {
    console.log(error);
    res.send("Ocurio un error");
  }
});

router.put("/:id", async function (req, res) {
  const { id } = req.params;
  const { cedula, nombre, tarea } = req.body;
  try {
  
    const result = await db.query(
      "UPDATE task SET  name = $1 , task = $2  WHERE id = $3 RETURNING *",
      [ nombre, tarea, id]
    );

    if (result.rows.length === 0)
      return res.status(404).json({ message: "Task not found" });

    return res.json(result.rows[0]);
  } catch (error) {
    console.log(error);
    res.send("Ocurio un error");
  }
});

router.delete("/:id", async function (req, res) {
  try {
    const { id } = req.params;
    const result = await db.query("DELETE FROM task WHERE id = $1", [id]);

    if (result.rowCount === 0)
      return res.status(404).json({ message: "Task not found" });
    return res.sendStatus(204);
  } catch (error) {
    console.log(error);
    res.send("Ocurio un error");
  }
});

module.exports = router;
