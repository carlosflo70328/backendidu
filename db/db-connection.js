const Pool = require('pg').Pool
const password = process.env.PASSWORD;
const db = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'UserTask',
  password: password,
  port: 5432,
})

module.exports = db;